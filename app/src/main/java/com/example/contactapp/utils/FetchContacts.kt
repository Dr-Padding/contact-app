package com.example.contactapp.utils

import android.content.ContentUris
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.ContactsContract
import com.example.contactapp.model.Contact
import java.io.File
import java.io.FileOutputStream

class FetchContacts(private val context: Context) {

    fun fetchContacts(): List<Contact> {

        val contentResolver = context.contentResolver

        val cursor = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )

        val contacts = mutableListOf<Contact>()
        cursor?.use {
            val nameIndex = it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            val phoneIndex = it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val contactIdIndex =
                it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)

            while (it.moveToNext()) {
                val name = if (nameIndex >= 0) it.getString(nameIndex) else ""
                val phone = if (phoneIndex >= 0) it.getString(phoneIndex) else ""
                val contactId = if (contactIdIndex >= 0) it.getLong(contactIdIndex) else -1
                val photoUri = getContactPhotoUri(contactId)

                val contact = Contact(name, phone, photoUri)
                contacts.add(contact)
            }
        }
        cursor?.close()
        return contacts
    }

    private fun getContactPhotoUri(contactId: Long): Uri? {
        val contentResolver = context.contentResolver
        val photoUri = ContentUris.withAppendedId(
            ContactsContract.Contacts.CONTENT_URI,
            contactId
        )
        val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
            contentResolver,
            photoUri
        )
        return if (inputStream != null) {
            val bitmap = BitmapFactory.decodeStream(inputStream)
            inputStream.close()
            val file = File(context.cacheDir, "contact_photo_${contactId}.jpg")
            val fileOutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
            fileOutputStream.close()
            Uri.fromFile(file)
        } else {
            null
        }
    }
}