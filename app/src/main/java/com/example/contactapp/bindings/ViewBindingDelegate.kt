package com.example.contactapp.bindings

import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


inline fun <reified T : ViewBinding> AppCompatActivity.viewBinding() =
    ActivityViewBindingDelegate(T::class.java)


class ActivityViewBindingDelegate<T : ViewBinding>(private val bindingClass: Class<T>) :
    ReadOnlyProperty<AppCompatActivity, T> {

    private var binding: T? = null

    override fun getValue(thisRef: AppCompatActivity, property: KProperty<*>): T {
        if (binding == null) {
            val inflater = LayoutInflater.from(thisRef)
            val method = bindingClass.getMethod("inflate", LayoutInflater::class.java)
            @Suppress("UNCHECKED_CAST")
            binding = method.invoke(null, inflater) as T
            thisRef.setContentView(binding?.root!!)
        }
        return binding!!
    }
}


inline fun <reified T : ViewBinding> Fragment.viewBinding(noinline viewBindingInflater: (LayoutInflater) -> T) =
    FragmentViewBindingDelegate(viewBindingInflater)


class FragmentViewBindingDelegate<T : ViewBinding>(
    private val viewBindingInflater: (LayoutInflater) -> T
) : ReadOnlyProperty<Fragment, T> {

    private var binding: T? = null

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        if (binding == null) {
            binding = viewBindingInflater.invoke(thisRef.layoutInflater)
        }
        return binding!!
    }
}
