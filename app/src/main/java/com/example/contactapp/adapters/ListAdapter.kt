package com.example.contactapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.example.contactapp.R
import com.example.contactapp.databinding.ItemContactBinding
import com.example.contactapp.model.Contact


class ListAdapter(
    private val context: Context,
    private var contacts: List<Contact>
) : BaseAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return contacts.size
    }

    override fun getItem(position: Int): Any {
        return contacts[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val holder: ViewHolder

        if (view == null) {
            val binding = ItemContactBinding.inflate(inflater, parent, false)
            view = binding.root
            holder = ViewHolder(binding)
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        val contact = contacts[position]
        holder.binding.tvContactName.text = contact.name
        holder.binding.tvPhoneNumber.text = contact.phoneNumber

        val photoUri = contact.imageUri
        if (photoUri != null) {
            Glide.with(context)
                .load(photoUri)
                .into(holder.binding.ivContactImage)
        } else {
            holder.binding.ivContactImage.setImageResource(R.drawable.default_contact_image)
        }

        view.setOnClickListener {
            val phoneNumber = contact.phoneNumber
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phoneNumber")
            context.startActivity(intent)
        }

        return view
    }

    fun updateContacts(newContacts: List<Contact>) {
        contacts = newContacts
        notifyDataSetChanged()
    }

    private class ViewHolder(val binding: ItemContactBinding)
}
