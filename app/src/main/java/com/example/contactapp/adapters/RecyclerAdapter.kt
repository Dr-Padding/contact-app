package com.example.contactapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.contactapp.R
import com.example.contactapp.databinding.ItemContactBinding
import com.example.contactapp.model.Contact

class RecyclerAdapter(
    private val context: Context,
    contacts: List<Contact>,
    private val clickListener: (Contact) -> Unit
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var contactList : List<Contact> = contacts

    private val differCallback = object : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.phoneNumber == newItem.phoneNumber
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = ItemContactBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact = contactList[position]
        holder.bind(contact, clickListener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: ItemContactBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(contact: Contact, clickListener: (Contact) -> Unit) {
            binding.tvContactName.text = contact.name
            binding.tvPhoneNumber.text = contact.phoneNumber

            val photoUri = contact.imageUri
            if (photoUri != null) {
                Glide.with(binding.root.context)
                    .load(photoUri)
                    .into(binding.ivContactImage)
            } else {
                binding.ivContactImage.setImageResource(R.drawable.default_contact_image)
            }

            binding.root.setOnClickListener { clickListener(contact) }
        }
    }
}
