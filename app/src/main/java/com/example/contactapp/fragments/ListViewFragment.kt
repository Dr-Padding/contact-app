package com.example.contactapp.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.contactapp.adapters.ListAdapter
import com.example.contactapp.bindings.viewBinding
import com.example.contactapp.databinding.FragmentListViewBinding
import com.example.contactapp.utils.FetchContacts


class ListViewFragment : Fragment() {

    private val binding by viewBinding(FragmentListViewBinding::inflate)
    private lateinit var adapter: ListAdapter
    private lateinit var fetchContacts: FetchContacts

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchContacts = FetchContacts(requireActivity())
        adapter = ListAdapter(requireContext(), mutableListOf())
        binding.listViewContacts.adapter = adapter

        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
                if (isGranted) {
                    loadContacts()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Permission Denied",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
        } else {
            loadContacts()
        }
    }

    private fun loadContacts() {
        val contacts = fetchContacts.fetchContacts()
        adapter.updateContacts(contacts)
    }
}