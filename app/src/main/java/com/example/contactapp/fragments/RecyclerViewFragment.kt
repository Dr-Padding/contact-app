package com.example.contactapp.fragments

import android.Manifest
import android.content.ContentUris
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.contactapp.adapters.RecyclerAdapter
import com.example.contactapp.bindings.viewBinding
import com.example.contactapp.databinding.FragmentRecyclerViewBinding
import com.example.contactapp.model.Contact
import com.example.contactapp.utils.FetchContacts
import java.io.File
import java.io.FileOutputStream


class RecyclerViewFragment : Fragment() {

    private val binding by viewBinding(FragmentRecyclerViewBinding::inflate)
    private lateinit var adapter: RecyclerAdapter
    private lateinit var fetchContacts: FetchContacts
    private lateinit var contactList: List<Contact>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchContacts = FetchContacts(requireActivity())
        binding.recyclerViewContacts.layoutManager = LinearLayoutManager(requireContext())

        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                if (isGranted) {
                    fetchContactsAndSetupAdapter()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Permission denied, cannot display contacts",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            ) == PackageManager.PERMISSION_GRANTED -> {
                fetchContactsAndSetupAdapter()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS) -> {
                requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
            }
            else -> {
                requestPermissionLauncher.launch(Manifest.permission.READ_CONTACTS)
            }
        }
    }

    private fun fetchContactsAndSetupAdapter() {
        contactList = fetchContacts.fetchContacts()
        adapter = RecyclerAdapter(requireContext(), contactList) { contact ->
            val intent = Intent(Intent.ACTION_DIAL).apply {
                data = Uri.parse("tel:${contact.phoneNumber}")
            }
            startActivity(intent)
        }
        binding.recyclerViewContacts.adapter = adapter
        adapter.differ.submitList(contactList)
    }
}
